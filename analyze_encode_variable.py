#!/usr/bin/env python3

import timeit
import time
import os
import csv
import libpysteg as pys

DATA = './data/wav/mono/'

setup = '''
import libpysteg as pys
encoder = pys.StegoEncoder('{}', 'test.txt')
'''

with open('lsb_encode_timings.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_lsb('./data/out/lsb/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=5)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse(path, './data/out/lsb/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr(path, './data/out/lsb/{0:0=4d}.wav'.format(i)))])

with open('fft_encode_timings.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_fft('./data/out/fft/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=5)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse(path, './data/out/fft/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr(path, './data/out/fft/{0:0=4d}.wav'.format(i)))])

with open('hybrid_encode_timings.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_hybrid('./data/out/hybrid/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=5)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse(path, './data/out/hybrid/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr(path, './data/out/hybrid/{0:0=4d}.wav'.format(i)))])
## EOF ##
