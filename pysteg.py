#!/usr/bin/env python3

import PySimpleGUI as sg
import libpysteg as pys
import sys
import traceback

import queue
import threading
import time

import hashlib

sg.theme('Light Blue 2')

gui_queue = queue.Queue()

encode_params = [[sg.Text('Cover Audio', size=(15, 1)), sg.Input(key='cover_path'),
              sg.FileBrowse(file_types=(('Waveform Audio', '*.wav'),))],
          [sg.Text('Secret Data', size=(15, 1)), sg.Input(key='secret_path'), sg.FileBrowse()],
          [sg.Text('Encode Output', size=(15,1)),
              sg.Input(key='output_path'), sg.FileSaveAs()]]

decode_params = [[sg.Text('Encoded Audio', size=(15, 1)), sg.Input(key='encoded_audio'),
            sg.FileBrowse()], [sg.Text('Decode Output', size=(15,1)),
                sg.Input(key='dec_output_path'), sg.FileSaveAs()]]

common_params = [[sg.Text('Key', size=(15,1)), sg.Input(key='key')]]

merged_params = [[sg.Frame('Decode Parameters', decode_params)],
                 [sg.Frame('Common Parameters', common_params)]]

bottom = [[sg.Multiline(size=(150,30), key='out', autoscroll=True,
                disabled=True)], [sg.Button('Encode'), sg.Button('Decode'),
                    sg.Radio('LSB', 'type', key='lsb', default=True),
                    sg.Radio('FFT', 'type', key='fft'), sg.Radio('Hybrid', 'type',
                    key='hybrid')]]


layout = [[sg.Frame('Encode Parameters', encode_params), sg.Column(merged_params)],
            [sg.Column(bottom)]]

window = sg.Window('PySteg', layout, finalize=True)
sg.cprint_set_output_destination(window, 'out')

def process_encode(values, gui_queue):
    try:
        encoder = pys.StegoEncoder(values['cover_path'], values['secret_path'])
        gui_queue.put('Encoding...')
        if values['lsb']:
            encoder.encode_lsb(values['output_path'], values['key'])
        elif values['fft']:
            encoder.encode_fft(values['output_path'], values['key'])
        elif values['hybrid']: 
            encoder.encode_hybrid(values['output_path'], values['key'])
        gui_queue.put('Encoding Complete!')
        gui_queue.put('SHA256sum: {}'.format(encoder.sha_sum))
        gui_queue.put('MSE: {}'.format(pys.mse(values['cover_path'],
            values['output_path'])))
        gui_queue.put('PSNR: {} dB'.format(pys.psnr(values['cover_path'],
            values['output_path'])))
        gui_queue.put('-----------------------------------------------')
    except Exception as e:
        gui_queue.put(e)
        return -1

def process_decode(values, gui_queue):
    try:
        decoder = pys.StegoDecoder(values['encoded_audio'])
        gui_queue.put('Decoding...')
        if values['lsb']:
            decoder.decode_lsb(values['dec_output_path'], values['key'])
        elif values['fft']:
            decoder.decode_fft(values['dec_output_path'], values['key'])
        if values['hybrid']:
            decoder.decode_hybrid(values['dec_output_path'], values['key'])
        gui_queue.put('Decoding Complete!')
        gui_queue.put('SHA256sum: {}'.format(hashlib.sha256(bytearray(
                                        open(values['dec_output_path'],
                                            mode='rb').read())).hexdigest()))
        gui_queue.put('-----------------------------------------------')
    except Exception as e:
        gui_queue.put(e)
        return -1 

def listen(gui_queue):
    if not gui_queue.empty():    
        result = gui_queue.get(False)
        sg.cprint(result)

while True:
    try:
        event, values = window.read(timeout=100)
        if event in (None, 'Exit'):
            break
        if event == 'Encode':
            threading.Thread(target=process_encode, args=(values, gui_queue,), daemon=True).start()
        if event == 'Decode':
            threading.Thread(target=process_decode, args=(values, gui_queue,), daemon=True).start()
        listen(gui_queue)
    except Exception as e:
        sg.cprint(e, text_color='red')
        continue
window.close()

## EOF ##
