#!/usr/bin/env python3

import timeit
import time
import os
import csv
import libpysteg as pys

DATA = './data/secret'

setup = '''
import libpysteg as pys
encoder = pys.StegoEncoder('./data/wav/mono/1044.wav', '{}')
'''

with open('lsb_encode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_lsb('./data/out/lsb/const/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse('./data/wav/mono/1044.wav', './data/out/lsb/const/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr('./data/wav/mono/1044.wav', './data/out/lsb/const/{0:0=4d}.wav'.format(i)))])

with open('fft_encode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_fft('./data/out/fft/const/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse('./data/wav/mono/1044.wav', './data/out/fft/const/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr('./data/wav/mono/1044.wav', './data/out/fft/const/{0:0=4d}.wav'.format(i)))])

with open('hybrid_encode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("encoder.encode_hybrid('./data/out/hybrid/const/{0:0=4d}.wav', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time),
            '{:.6f}'.format(pys.mse('./data/wav/mono/1044.wav', './data/out/hybrid/const/{0:0=4d}.wav'.format(i))),
            '{:.6f}'.format(pys.psnr('./data/wav/mono/1044.wav', './data/out/hybrid/const/{0:0=4d}.wav'.format(i)))])
## EOF ##
