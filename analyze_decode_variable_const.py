#!/usr/bin/env python3

import timeit
import time
import os
import csv
import libpysteg as pys

DATA = './data/out/{}/const/'

setup = '''
import libpysteg as pys
decoder = pys.StegoDecoder('{}')
'''

with open('lsb_decode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA.format('lsb')):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("decoder.decode_lsb('./data/decode/lsb/const/{0:0=4d}', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time)])

with open('fft_decode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA.format('fft')):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("decoder.decode_fft('./data/decode/fft/const/{0:0=4d}', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time)])

with open('hybrid_decode_timings_const.csv', mode='w') as fd:
    writer = csv.writer(fd, delimiter=',', quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
    
    paths = []

    for entry in os.scandir(DATA.format('hybrid')):
        if entry.is_file():
            paths.append(entry.path)

    paths.sort()

    for i, path in enumerate(paths):
        time = timeit.Timer("decoder.decode_hybrid('./data/decode/hybrid/const/{0:0=4d}', 'testkey')".format(i),
            setup=setup.format(path)).timeit(number=1)
        writer.writerow(['{0:0=4d}'.format(i), 
            os.path.getsize(path),
            '{:.6f}'.format(time)])
## EOF ##
