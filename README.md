# Hiding in Plain Sound: A Cryptographic Approach to Multi-Domain Audio Steganography 🕵

`pysteg` and `libpysteg` compose the user interface and reference implementation
of the algorithms described in my paper. This project was done for CSE 374,
section C, Fall 2020.

# Installation

Simply run `pip install -r requirements.txt`

# Operation

Run `pysteg.py` for the user interface. For importing as a module, use
`libpysteg.py`. The `analyze_*.py` files were used to perform batch tests.

# Verification

The files in `test/` can be used to verify, and to hear sound quality (as I
mentioned in the video presentation). Simply input them into the gui program, select
the corresponding method (LSB, FFT, or Hybrid), provide a name for the output file,
provide `cse374hello` (CASE SENSITIVE!) as the key, and
hit decode. You should end up with the contents of `hello.txt` in all three
output files.
