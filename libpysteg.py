#!/usr/bin/env python3
'''LIBPYSTEG

This module provides the necessary functions to steganographically encode and
decode arbitrary data.

This file can be imported as a module, containing the following functions for
public use:
    * get_bit_array - converts an array of bytes into an array of bits
    * get_byte_array - converts an array of bits into an array of bytes
    * psnr - computes the Peak Signal-to-Noise Ratio (PSNR) for a given corrupted audio
            file.
    * mse - computes the Mean Squared Error (MSE) for a given corrupted audio
            file.

And the following classes:
    * StegoEncoder - represents the steganographic encode process
    * StegoDecoder - represents the steganographic decode process
'''

import numpy as np
import scipy.io.wavfile as wv
import scipy.fft as fft
import random
import hashlib

class StegoEncoder:
    '''A class used to represent the steganographic encode process.

    Attributes
    ----------
    rate : int
        the sampling rate of the cover audio
    cover_audio : np.ndarray(dtype=np.uint8 || dtype=np.int16 ||
                    dtype=np.float32, ndim=1)
        the cover audio raw data
    secret_data : bytearray
        the data-to-be-hidden
    rng : Random
        static random number generator
    sha_sum : int
        the sha256 digest of secret_data
    
    Methods
    -------
    encode_lsb(key=None)
        Encodes secret_data within cover_audio using the least significant bit
        (LSB) technique.
    encode_fft(key=None)
        Encodes secret_data within cover_audio using the fast Fourier transform
        (FFT) technique.
    encode_hybrid(key=None)
        Encodes secret_data within cover_audio using LSB for the first half, and
        FFT for the second half.
    '''
    
    '''MAGIC NUMBERS'''
    HEADER_BYTE_LENGTH = 4
    F_MIN = 18000
    '''MAGIC NUMBERS'''

    def __init__(self, cover_path: str, secret_path: str):
        '''
        Parameters
        ----------
        cover_path : str
            the path, relative or absolute, to the cover audio
        secret_path : str
            the path, relative or absolute, to the secret data

        Raises
        ------
        Exception
            If the cover audio is not large enough to hold the secret data.
        Exception 
            If the cover audio is not mono.
        '''
        self.rate, self.cover_audio = wv.read(cover_path)

        if self.cover_audio.ndim != 1:
            raise Exception('Audio must be mono. Please downmix to mono '\
                                'or try another file. Number of channels '\
                                'in the supplied file: '\
                                '{}'.format(self.cover_audio.ndim))

        self.secret_data = bytearray(open(secret_path, mode='rb').read())
        
        if len(self.cover_audio) < len(self.secret_data) * 8 + self.HEADER_BYTE_LENGTH * 16:
            raise Exception('Cover audio must be longer than secret data by a '\
                                'factor of 8. {} more bytes are '\
                                'required.'.format(abs(len(self.cover_audio) -
                                len(self.secret_data) * 8 -
                                self.HEADER_BYTE_LENGTH * 16)))

        self.rng = random.Random()
        self.sha_sum = hashlib.sha256(self.secret_data).hexdigest()

    def encode_lsb(self, output_path: str, key: str = None) -> None:
        '''
        Encodes secret_data within cover_audio using the least significant bit
        (LSB) technique.

        If the argument `key` is not passed, the SHA256 digest of secret_data is
        used.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the encoded audio
        key : str, optional
            the key used by the encoding process
        '''

        if key is None:
            self.rng.seed(self.sha_sum)
        else:
            self.rng.seed(key)

        secret_data_with_hdr = bytearray(self.generate_header(self.secret_data) +
                self.secret_data)

        secret_data_bits = get_bit_array(secret_data_with_hdr)

        encoded_audio = self.cover_audio.copy()

        rand_seq = self.rng.sample(range(len(encoded_audio)),
                len(secret_data_bits))

        for i, bit in enumerate(secret_data_bits):
            encoded_audio[rand_seq[i]] = (encoded_audio[rand_seq[i]] & 0xFFFE) | bit
        
        wv.write(output_path, self.rate, encoded_audio)

    def _encode_lsb(self, key: str = None) -> np.ndarray:
        if key is None:
            self.rng.seed(self.sha_sum)
        else:
            self.rng.seed(key)
        
        secret_data = self.secret_data[:len(self.secret_data)//2]

        secret_data_with_hdr = bytearray(self.generate_header(secret_data)
                + secret_data)

        secret_data_bits = get_bit_array(secret_data_with_hdr)

        encoded_audio = self.cover_audio.copy()[:len(self.cover_audio)//2] 

        rand_seq = self.rng.sample(range(len(encoded_audio)),
                len(secret_data_bits))

        for i, bit in enumerate(secret_data_bits):
            encoded_audio[rand_seq[i]] = (encoded_audio[rand_seq[i]] & 0xFFFE) | bit
       
        encoded_audio = np.float32(encoded_audio) / np.max(encoded_audio)

        return encoded_audio

    def encode_fft(self, output_path: str, key: str = None) -> None:
        '''
        Encodes secret_data within cover_audio using the fast Fourier transform
        (FFT) technique.

        If the argument `key` is not passed, the SHA256 digest of secret_data is
        used.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the encoded audio
        key : str, optional
            the key used by the encoding process
        '''

        if key is None:
            self.rng.seed(self.sha_sum)
        else:
            self.rng.seed(key)
        
        secret_data_with_hdr = bytearray(self.generate_header(self.secret_data) +
                self.secret_data)

        secret_data_bits = get_bit_array(secret_data_with_hdr)    
        
        encoded_audio = self.cover_audio.copy()
        
        transform = fft.dct(np.float32(encoded_audio), norm='ortho')
        
        bins = np.arange(len(transform)) * self.rate / len(transform)
        
        f_eps = np.searchsorted(bins, self.F_MIN, side='right')
        
        if len(transform) - f_eps < len(secret_data_bits):
            f_eps = len(transform) - len(secret_data_bits)

        rand_seq = self.rng.sample(range(f_eps, len(transform)), len(secret_data_bits))

        # IEEE floating-point standard allows for negative zero
        for i, bit in enumerate(secret_data_bits):
            if bit == 1:
                transform[rand_seq[i]] = -abs(transform[rand_seq[i]])
            else:
                transform[rand_seq[i]] = abs(transform[rand_seq[i]])

        encoded_audio = fft.idct(transform, norm='ortho')

        encoded_audio = encoded_audio / np.max(encoded_audio)

        wv.write(output_path, self.rate, encoded_audio)

    def _encode_fft(self, key: str = None) -> np.ndarray:
        if key is None:
            self.rng.seed(self.sha_sum)
        else:
            self.rng.seed(key)
        
        secret_data = self.secret_data[len(self.secret_data)//2:]

        secret_data_with_hdr = bytearray(self.generate_header(secret_data) 
                + bytearray(int.to_bytes(int(np.max(self.cover_audio[:len(self.cover_audio)//2])),
                    self.HEADER_BYTE_LENGTH, byteorder='big')) + secret_data)

        secret_data_bits = get_bit_array(secret_data_with_hdr)    
        
        encoded_audio = self.cover_audio.copy()[len(self.cover_audio)//2:]
        
        transform = fft.dct(np.float32(encoded_audio), norm='ortho')
        
        bins = np.arange(len(transform)) * self.rate / len(transform)
        
        f_eps = np.searchsorted(bins, self.F_MIN, side='right')
        
        if len(transform) - f_eps < len(secret_data_bits):
            f_eps = len(transform) - len(secret_data_bits)

        rand_seq = self.rng.sample(range(f_eps, len(transform)), len(secret_data_bits))
        
        # IEEE floating-point standard allows for negative zero
        for i, bit in enumerate(secret_data_bits):
            if bit == 1:
                transform[rand_seq[i]] = -abs(transform[rand_seq[i]])
            else:
                transform[rand_seq[i]] = abs(transform[rand_seq[i]])

        encoded_audio = fft.idct(transform, norm='ortho')

        encoded_audio = encoded_audio / np.max(encoded_audio)
        
        return encoded_audio

    def encode_hybrid(self, output_path: str, key: str = None) -> None:
        '''
        Encodes secret_data within cover_audio using LSB for the first half, and
        FFT for the second half.

        If the argument `key` is not passed, the SHA256 digest of secret_data is
        used.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the encoded audio
        key : str, optional
            the key used by the encoding process
        '''

        merged_audio = np.concatenate((self._encode_lsb(key), self._encode_fft(key)))
        wv.write(output_path, self.rate, merged_audio)

    def generate_header(self, secret_data: bytearray) -> bytearray:
        '''
        Generates a header of fixed length containing a big-endian integer
        representing the length of the secret_data in bytes.

        Parameters
        ----------
        key : secret_data
            the secret data for which a header should be generated
        '''
        return bytearray(int.to_bytes(len(secret_data), self.HEADER_BYTE_LENGTH, byteorder='big'))


class StegoDecoder:
    '''A class used to represent the steganographic decode process.

    Attributes
    ----------
    rate : int
        the sampling rate of the steganographically encoded audio
    encoded_audio : np.ndarray(dtype=np.uint8 || dtype=np.int16 ||
                    dtype=np.float32, ndim=1)
        the steganographically encoded audio raw data
    rng : Random
        static random number generator
    
    Methods
    -------
    decode_lsb(key)
        Decodes a secret file hidden within encoded_audio using the least significant bit
        (LSB) technique.
    decode_fft(key)
        Decodes a secret file hidden within encoded_audio using the fast Fourier transform
        (FFT) technique.
    decode_hybrid(key)
        Decodes a secret file hidden within encoded_audio using the hybrid
        technique.
    '''

    '''MAGIC NUMBERS'''
    HEADER_BIT_LENGTH = 32
    F_MIN = 18000
    '''MAGIC NUMBERS'''

    def __init__(self, encoded_path: str):
        '''
        Parameters
        ----------
        encoded_path : str
            the path, relative or absolute, to the steganographically
            encoded audio 
        '''
        self.rate, self.encoded_audio = wv.read(encoded_path)

        if self.encoded_audio.ndim != 1:
            raise Exception('Audio must be mono. Please downmix to mono '\
                                'or try another file. Number of channels '\
                                'in the supplied file: '\
                                '{}'.format(self.encoded_audio.ndim))

        self.rng = random.Random()

    def decode_lsb(self, output_path: str, key: str) -> None:
        '''
        Decodes a secret file hidden within encoded_audio using the least significant bit
        (LSB) technique.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the decoded data
        key : str
            the key that was used to encode the secret data
        '''

        if key == None:
            raise Exception('No key supplied for decoding. Please supply a key.')

        self.rng.seed(key)

        decoded_bits = []
        header_bits = []

        rand_hdr_seq = self.rng.sample(range(len(self.encoded_audio)),
                self.HEADER_BIT_LENGTH)

        for i in rand_hdr_seq:
            header_bits.append(self.encoded_audio[i] & 1)
        
        header_bytes = get_byte_array(header_bits)
        secret_len = int.from_bytes(header_bytes, byteorder='big') * 8
        
        rand_seq = self.rng.sample(range(len(self.encoded_audio)), secret_len)

        for i in rand_seq:
            decoded_bits.append(self.encoded_audio[i] & 1)

        decoded_bytes = get_byte_array(decoded_bits)

        with open(output_path, mode='wb') as fd:
            fd.write(decoded_bytes)
            fd.close()
    
    def _decode_lsb(self, key: str, max_val: int) -> np.ndarray:
        self.rng.seed(key)

        decoded_bits = []
        header_bits = []

        encoded_audio = np.int16(np.rint(self.encoded_audio[:len(self.encoded_audio)//2]
                * max_val))

        rand_hdr_seq = self.rng.sample(range(len(encoded_audio)),
                self.HEADER_BIT_LENGTH)

        for i in rand_hdr_seq:
            header_bits.append(encoded_audio[i] & 1)
        
        header_bytes = get_byte_array(header_bits)
        secret_len = int.from_bytes(header_bytes, byteorder='big') * 8
        
        rand_seq = self.rng.sample(range(len(encoded_audio)), secret_len)

        for i in rand_seq:
            decoded_bits.append(encoded_audio[i] & 1)
        
        decoded_bytes = get_byte_array(decoded_bits)
        
        return decoded_bytes

    def decode_fft(self, output_path: str, key: str) -> None:
        '''
        Decodes a secret file hidden within encoded_audio using the fast Fourier
        transform (FFT) technique.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the decoded data
        key : str
            the key that was used to encode the secret data
        '''

        self.rng.seed(key)

        decoded_bits = []
        header_bits = []
        
        transform = fft.dct(self.encoded_audio, norm='ortho')
        
        bins = np.arange(len(transform)) * self.rate / len(transform)
        
        f_eps = np.searchsorted(bins, self.F_MIN, side='right')

        rand_hdr_seq = self.rng.sample(range(f_eps, len(transform)), self.HEADER_BIT_LENGTH)
        
        for i in rand_hdr_seq:
            if np.signbit(transform[i]):
                header_bits.append(1)
            else:
                header_bits.append(0)
          
        header_bytes = get_byte_array(header_bits)
        secret_len = int.from_bytes(header_bytes, byteorder='big') * 8
        
        rand_seq = self.rng.sample(range(f_eps, len(transform)), secret_len)

        for i in rand_seq:
            if np.signbit(transform[i]):
                decoded_bits.append(1)
            else:
                decoded_bits.append(0)

        decoded_bytes = get_byte_array(decoded_bits)
        
        with open(output_path, mode='wb') as fd:
            fd.write(decoded_bytes)
            fd.close()
    
    def _decode_fft(self, key: str) -> (np.ndarray, int):
        self.rng.seed(key)

        decoded_bits = []
        header_bits = []
        
        transform = fft.dct(self.encoded_audio[len(self.encoded_audio)//2:], norm='ortho')
        
        bins = np.arange(len(transform)) * self.rate / len(transform)
        
        f_eps = np.searchsorted(bins, self.F_MIN, side='right')

        rand_hdr_seq = self.rng.sample(range(f_eps, len(transform)), 2 * self.HEADER_BIT_LENGTH)
        
        for i in rand_hdr_seq:
            if np.signbit(transform[i]):
                header_bits.append(1)
            else:
                header_bits.append(0)
          
        header_bytes = get_byte_array(header_bits)
        secret_len = int.from_bytes(header_bytes[0:4], byteorder='big') * 8
        max_val = int.from_bytes(header_bytes[4:8], byteorder='big')
        
        rand_seq = self.rng.sample(range(f_eps, len(transform)), secret_len)

        for i in rand_seq:
            if np.signbit(transform[i]):
                decoded_bits.append(1)
            else:
                decoded_bits.append(0)
        
        return (get_byte_array(decoded_bits), max_val)

    def decode_hybrid(self, output_path: str, key: str) -> None:
        '''
        Decodes a secret file hidden within encoded_audio using the hybrid
        technique.

        Parameters
        ----------
        output_path : str
            the path, relative or absolute, to output the decoded data
        key : str
            the key that was used to encode the secret data
        '''

        bytes_fft, max_val = self._decode_fft(key)
        bytes_lsb = self._decode_lsb(key, max_val)
        bytes_merged = bytes_lsb + bytes_fft

        with open(output_path, mode='wb') as fd:
            fd.write(bytes_merged)
            fd.close()

def get_bit_array(byte_array: bytearray) -> np.ndarray:
    '''
    Converts an array of bytes into an array of bits.

    Parameters
    ----------
    byte_array : bytearray
        an array of bytes to be converted to bits
    '''

    return np.unpackbits(np.fromiter(byte_array, dtype=np.uint8))

def get_byte_array(bit_array: np.ndarray) -> bytearray:
    '''
    Converts an array of bits into an array of bytes.

    Parameters
    ----------
    bit_array : np.ndarray
        an array of bits to be converted to bytes
    '''

    return bytearray(np.packbits(bit_array))

def psnr(audio_original_path: str, audio_corrupted_path: str) -> float:
    '''
    Computes the Peak Signal-to-Noise Ratio (PSNR) for a given corrupted audio
    file.

    Parameters
    ----------
    audio_original_path : str
        the path, relative or absolute, to the ground truth audio
    audio_corrupted_path : str
        the path, relative or absolute, to the corrupted audio
    '''

    audio_original = wv.read(audio_original_path)[1]
    
    if audio_original.dtype != np.float32:
        audio_original = np.float32(audio_original) / np.max(audio_original)

    audio_corrupted = wv.read(audio_corrupted_path)[1]

    if audio_corrupted.dtype != np.float32:
        audio_corrupted = np.float32(audio_corrupted) / np.max(audio_corrupted)

    mse = np.mean((audio_original - audio_corrupted) ** 2)
    
    if mse == 0:
        return float('inf')
    
    MAX_VAL = 1.0

    return 20 * np.log10(MAX_VAL) - 10 * np.log10(mse)

def mse(audio_original_path: str, audio_corrupted_path: str) -> float:
    '''
    Computes the Mean Squared Error (MSE) for a given corrupted audio
    file.

    Parameters
    ----------
    audio_original_path : str
        the path, relative or absolute, to the ground truth audio
    audio_corrupted_path : str
        the path, relative or absolute, to the corrupted audio
    '''

    audio_original = wv.read(audio_original_path)[1]
    
    if audio_original.dtype != np.float32:
        audio_original = np.float32(audio_original) / np.max(audio_original)

    audio_corrupted = wv.read(audio_corrupted_path)[1]

    if audio_corrupted.dtype != np.float32:
        audio_corrupted = np.float32(audio_corrupted) / np.max(audio_corrupted)

    mse = np.mean((audio_original - audio_corrupted) ** 2)
    
    return mse

## EOF ##
